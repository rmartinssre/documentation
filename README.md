# Documentation

## Architecture

![Desenho da Arquitetura](./img/arq.png)

## Why serverless framework? (in AWS)

Serverless is a simple method to deploy api`s on AWS (or another provider), wich provide agility for your development team.

With serverless, developer gets autonomy to make deploys at the application and infrastructure. 

## Application deploy

For the application deploy, you must have a serverless.yaml file in your project, wich contains the parameters based on cloudformation. 

1 - To deploy locally

- Configure aws credentials 
- Install node or npm
- Install serverless framework
- Run sls deploy 

https://www.serverless.com/framework/docs/providers/aws/cli-reference/config-credentials/

Thats It!

![lambdafunctions](./img/lambdafunctions.png)

2 - To deploy on GitLab CICD

- Configure environment variables for your project (https://docs.gitlab.com/ee/ci/variables/)
- Create a simple .gitlab-ci.yml file with one stage
- Use a public or private docker image, in order to execute DockerinDocker step for your deploy
- Run serverless deploy at the end of your gitlab-ci

Every commit, triggers a pipeline wich will make changes in your infrasctructure or in yor applcation

![Desenho da pipeline](./img/sls.png)

![gitlabci](./img/pipeline.png)


## Infrastructure deploy

The core infrastructure (vpc, clusters, VM`s, RDS Database), can be deployed in a different project. I'm using terrafom to provide some core resources for all applications in your enviroment.

To deploy terraform, you must configure aws credentials and download terraform. After that, just run your code in order to create your environment.

You can also create a pipeline to deploy your infrasctruture, every commit will trigger a pipeline wich will make changes in your environment.


## Metrics & Dashboards

The metrics for your applications can be visualized on cloudwatch. You can find a lot of metrics like numer of requests, errors, latency. In a aws serverlerss environment, this metrics are enable by default. You just have to create your dashboards.


![dash1](./img/dashboard1.png)

![dash1](./img/dashboard2.png)


## Logs

Logs from your application, are processed in cloudwatch log groups. They are created by default at the moment you deploy your servereless application. 

Each loggroup can be parsed to your log agregattor, like elasticsearch or graylog.

I choosen elasticsearch managed on aws, in order to make a parsing for my logs and create filters on kibana that make a better visualization

![loggroups](./img/loggroups.png)

![indexes](./img/indexes.png)

![filter](./img/kibanafilter.png)

## Parsing logs

In order to logs be parsed, you must configure a LambdaFunction, wich is trigged when a loggroup is processed. Each loggroup is processed as an index in elasticsearch. 

The node.js script posted at this documentation (LogsToElk.js), give you instructions to separate indexes for each loggroup.

![streaming](./img/LogStreaming.png)








